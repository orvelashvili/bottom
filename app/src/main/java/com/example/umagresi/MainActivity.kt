package com.example.umagresi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.umagresi.fragment.DashboardFragment
import com.example.umagresi.fragment.HomeFragment
import com.example.umagresi.fragment.NotificationFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportActionBar?.hide()


        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.buttonNavView)

        val controller = findNavController(R.id.nav_host_fragment)

        val appBarConfig = AppBarConfiguration(setOf(
            R.id.homeFragment,
            R.id.dashboardFragment,
            R.id.notificationFragment,
            R.id.infoFragment

        ))

        setupActionBarWithNavController(controller, appBarConfig)
        bottomNavigationView.setupWithNavController(controller)



    }

}